#include "retangulo.hpp"

retangulo::retangulo(){

	setBase(10);
	setAltura(10);
}

retangulo::retangulo(float base, float altura){
    setBase(base);
    setAltura(altura);
}

float retangulo::area(){
	return getBase() * getAltura();
}

float retangulo::area(float base, float altura){
	return base * altura;
}
