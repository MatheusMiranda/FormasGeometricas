#include "retangulo.hpp"
#include "quadrado.hpp"
#include "triangulo.hpp"

#include <iostream>

using namespace std;

int main(){

	geometrica * formaGeometrica = new retangulo(20,10);
    cout << "Calculo da Area do retangulo:" << formaGeometrica->area() << endl;

	geometrica * formaGeometrica2 = new quadrado(15,15);
	cout << "Calculo da Area do quadrado:" << formaGeometrica2->area() << endl;
	
	geometrica * formaGeometrica3 = new triangulo(20,15);
	cout << "Calculo da Area do triangulo:" << formaGeometrica3->area() << endl;
	
	
	return 0;

}
