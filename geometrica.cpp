#include "geometrica.hpp"

geometrica::geometrica(){

	setBase(10);
	setAltura(10);
}

geometrica::geometrica(float base, float altura){
    //this -> base = base;
	//this -> altura = altura; se eu fizesse assim n prescisaria das linhas de baixo;
	setBase(base);
	setAltura(altura);

}

float geometrica::getBase(){

	return base;

}

float geometrica::getAltura(){

	return altura;

}

void geometrica::setBase(float base){

	this->base = base;

}

void geometrica::setAltura(float altura){

	this->altura = altura;

}
