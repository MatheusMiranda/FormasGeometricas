#include "quadrado.hpp"

quadrado::quadrado(){
	
	setBase(10);
	setAltura(10);

}

quadrado::quadrado(float base,float altura){
	
	setBase(base);
	setAltura(altura);

}

float quadrado::area(){
	
 	return getBase() * getAltura();

}

float quadrado::area(float base, float altura){

	return base * altura;

}




