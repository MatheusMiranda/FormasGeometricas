#ifndef GEOMETRICA_H
#define GEOMETRICA_H

class geometrica{
private: float base, altura;

public: geometrica();
		geometrica(float base, float altura);

		float getBase();
		float getAltura();
		void setBase(float base);
		void setAltura(float altura);

		virtual float area() = 0;
	


};
#endif
