#ifndef TRIANGULO_H
#define TRIANGULO_H

#include "geometrica.hpp"

class triangulo : public geometrica{
	
public: triangulo();
		triangulo(float base, float altura);
		float area();
		float area(float base, float altura);

};

#endif
